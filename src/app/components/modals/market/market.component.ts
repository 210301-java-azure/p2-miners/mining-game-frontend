import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { MinerService } from 'src/app/services/miner.service';
import { MineComponent } from '../../mine/mine.component';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.css'],
})
export class MarketComponent implements OnInit {
  mineTypesToSelect: string[] = ['OIL', 'IRON', 'COPPER'];
  mineTypeToSell: string = '-';
  mineTypeToBuy: string = '-';

  sellAmount: string = '';

  tradeErrMsg?: string;

  constructor(
    private appComponent: AppComponent,
    private mineComponent: MineComponent,
    private minerService: MinerService
  ) {}

  sellAmountToNum(
    sellAmount: string,
    mineTypeToSell: string,
    mineTypeToBuy: string
  ): number {
    switch (mineTypeToSell) {
      case 'OIL':
        if (mineTypeToBuy === 'IRON') return +sellAmount * 5;
        if (mineTypeToBuy === 'COPPER') return +sellAmount * 10;
        return 0;
      case 'IRON':
        if (mineTypeToBuy === 'OIL') return +sellAmount * 0.2;
        if (mineTypeToBuy === 'COPPER') return +sellAmount * 2;
        return 0;
      case 'COPPER':
        if (mineTypeToBuy === 'OIL') return +sellAmount * 0.1;
        if (mineTypeToBuy === 'IRON') return +sellAmount * 0.5;
        return 0;
      default:
        return 0;
    }
  }

  getTradeRate(mineType: string): number {
    switch (mineType) {
      case 'OIL':
        return 1;
      case 'IRON':
        return 5;
      case 'COPPER':
        return 10;
      default:
        return 0;
    }
  }

  clickToExit(): void {
    this.mineComponent.selectedSidebar = '';
  }

  clickTrade(): void {
    if (this.mineTypeToSell === '-' || this.mineTypeToBuy === '-') {
      this.tradeErrMsg = 'Please specify resources to trade';
      return;
    }
    this.minerService
      .tradeResouces(
        this.appComponent.minerId || 0,
        sessionStorage.getItem('token') || '',
        this.mineTypeToSell,
        +this.sellAmount,
        this.mineTypeToBuy
      )
      .subscribe(
        (res) => {
          this.tradeErrMsg = '';
          this.mineTypeToSell = '-';
          this.mineTypeToBuy = '-';
          this.sellAmount = '';

          this.appComponent.updateUserResources(
            res.body.oil,
            res.body.iron,
            res.body.copper
          );
        },
        (res) => {
          this.tradeErrMsg = res.error.message;
        }
      );
  }

  ngOnInit(): void {}
}
