import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotTorsoComponent } from './robot-torso.component';

describe('RobotTorsoComponent', () => {
  let component: RobotTorsoComponent;
  let fixture: ComponentFixture<RobotTorsoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RobotTorsoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotTorsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
