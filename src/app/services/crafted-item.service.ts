import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CraftedItemService {
  itemUrl = environment.baseUrl + '/items';

  constructor(private http: HttpClient) {}

  getAllItems(): Observable<any> {
    return this.http.get<any>(this.itemUrl, { observe: 'response' });
  }

  getAllItemsByMinerId(minerId: number): Observable<any> {
    return this.http.get<any>(`${this.itemUrl}?miner-id=${minerId}`, {
      observe: 'response',
    });
  }

  craftItem(minerId: number, itemName: string): Observable<any> {
    return this.http.get<any>(
      `${this.itemUrl}/${itemName}/craft?miner-id=${minerId}`,
      { observe: 'response' }
    );
  }
}
