import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { ProfileComponent } from './components/profile/profile.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { MineComponent } from './components/mine/mine.component';
import { HomeComponent } from './components/home/home.component';
import { CraftComponent } from './components/modals/craft/craft.component';
import { LoginComponent } from './components/forms/login/login.component';
import { RegisterComponent } from './components/forms/register/register.component';
import { MarketComponent } from './components/modals/market/market.component';
import { InventoryComponent } from './components/modals/inventory/inventory.component';
import { CraftItemCardComponent } from './components/craft-item-card/craft-item-card.component';
import { InventoryCardComponent } from './components/inventory-card/inventory-card.component';
import { DrillComponent } from './components/svg-icons/drill/drill.component';
import { OilComponent } from './components/svg-icons/oil/oil.component';
import { IronComponent } from './components/svg-icons/iron/iron.component';
import { CopperComponent } from './components/svg-icons/copper/copper.component';
import { ResourceCartComponent } from './components/svg-icons/resource-cart/resource-cart.component';
import { TarpsComponent } from './components/svg-icons/tarps/tarps.component';
import { PickaxeComponent } from './components/svg-icons/pickaxe/pickaxe.component';
import { CharacterComponent } from './components/svg-icons/character/character.component';
import { TruckComponent } from './components/svg-icons/truck/truck.component';
import { RobotComponent } from './components/svg-icons/robot/robot.component';
import { RobotArmComponent } from './components/svg-icons/robot-arm/robot-arm.component';
import { RobotTorsoComponent } from './components/svg-icons/robot-torso/robot-torso.component';
import { RobotHeadComponent } from './components/svg-icons/robot-head/robot-head.component';
import { RobotLegComponent } from './components/svg-icons/robot-leg/robot-leg.component';
import { MountainsComponent } from './components/svg-icons/mountains/mountains.component';
import { LoadingComponent } from './components/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ProfileComponent,
    LeaderboardComponent,
    MineComponent,
    HomeComponent,
    CraftComponent,
    LoginComponent,
    RegisterComponent,
    MarketComponent,
    InventoryComponent,
    CraftItemCardComponent,
    InventoryCardComponent,
    DrillComponent,
    OilComponent,
    IronComponent,
    CopperComponent,
    ResourceCartComponent,
    TarpsComponent,
    PickaxeComponent,
    CharacterComponent,
    TruckComponent,
    RobotComponent,
    RobotArmComponent,
    RobotTorsoComponent,
    RobotHeadComponent,
    RobotLegComponent,
    MountainsComponent,
    LoadingComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
