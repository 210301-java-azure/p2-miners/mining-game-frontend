import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MinerService {
  minerUrl = environment.baseUrl + '/miners';
  constructor(private http: HttpClient) {}

  updateClickedResource(
    minerId: number,
    clickedAmount: number
  ): Observable<any> {
    clickedAmount = Math.round(clickedAmount * 100) / 100;
    return this.http.get<any>(
      this.minerUrl + `/${minerId}/add?resource-amount=${clickedAmount}`,
      { observe: 'response' }
    );
  }

  tradeResouces(
    minerId: number,
    token: string,
    supplyType: string,
    supplyAmount: number,
    returnType: string
  ): Observable<any> {
    return this.http.get<any>(
      this.minerUrl +
        `/${minerId}/trade?supply-type=${supplyType}&supply-amount=${supplyAmount}&return-type=${returnType}`,
      { headers: { 'miner-token': token }, observe: 'response' }
    );
  }

  getMinerById(minerId: number, token: string): Observable<any> {
    return this.http.get(`${this.minerUrl}/${minerId}`, {
      headers: { 'miner-token': token },
      observe: 'response',
    });
  }

  getWeatherByMinerId(minerId: number, token: string): Observable<any> {
    return this.http.get(`${this.minerUrl}/${minerId}/weather`, {
      headers: { 'miner-token': token },
      observe: 'response',
    });
  }

  getEfficiencyByMinerId(minerId: number, token: string): Observable<any> {
    return this.http.get<any>(`${this.minerUrl}/${minerId}/efficiency`, {
      headers: { 'miner-token': token },
      observe: 'response',
    });
  }

  getLeaderboard(): Observable<any> {
    return this.http.get(`${this.minerUrl}/leaderboard`, {
      observe: 'response',
    });
  }
}
