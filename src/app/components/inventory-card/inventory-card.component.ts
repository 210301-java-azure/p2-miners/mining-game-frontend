import { Component, Input, OnInit } from '@angular/core';
import { CraftedItem } from 'src/app/models/CraftedItem';
import { Findable } from 'src/app/models/Findable';

@Component({
  selector: 'app-inventory-card',
  templateUrl: './inventory-card.component.html',
  styleUrls: ['./inventory-card.component.css'],
})
export class InventoryCardComponent implements OnInit {
  @Input() item?: CraftedItem;
  @Input() inventoryItemQuantity?: number;
  @Input() findable?: Findable;

  isDescriptionShown: boolean = false;

  constructor() {}

  showDescription() {
    this.isDescriptionShown = true;

    setTimeout(() => {
      this.isDescriptionShown = false;
    }, 3000);
  }

  ngOnInit(): void {}
}
