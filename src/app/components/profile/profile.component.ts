import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { JWT } from 'src/app/jwt-util';
import { AuthService } from 'src/app/services/auth.service';
import { MinerService } from 'src/app/services/miner.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  isUpdateModalOpen: boolean = false;
  isDeleteModalOpen: boolean = false;
  confirmPasswordToDelete: string = '';
  password: string | null = null;
  username: string = '';
  email: string | null = null;
  zipcode: string | null = null;
  mineType: string = '';

  isLoading: boolean = false;

  deleteModalErrMsg: string = '';
  updateModalErrMsg: string = '';
  deleteModalSuccessMsg: string = '';
  updateModalSuccessMsg: string = '';
  isDeleted: boolean = false;
  isUpdated: boolean = false;

  constructor(
    public appComponent: AppComponent,
    private userService: UserService,
    private authService: AuthService,
    private minerService: MinerService
  ) {}

  clickLogout(): void {
    sessionStorage.removeItem('token');
    this.appComponent.isLoggedIn = false;
    this.appComponent.resetFieldsUponLogout();
    this.appComponent.isMineComponentInitialized = false;
  }
  clickUpdateButton(): void {
    this.isUpdated = false;
    this.isUpdateModalOpen = true;
  }

  clickDeleteAccount(): void {
    this.isDeleteModalOpen = true;
  }

  exitUpdateModal(): void {
    this.updateModalSuccessMsg = '';
    this.updateModalErrMsg = '';
    this.isUpdateModalOpen = false;
  }

  exitDeleteModal(): void {
    this.isDeleteModalOpen = false;
  }

  deleteUserForever(id: number): void {
    this.userService
      .deleteUserById(id, sessionStorage.getItem('token') || '')
      .subscribe(
        (res) => {
          this.confirmPasswordToDelete = '';
          this.isDeleted = true;
          sessionStorage.removeItem('token');
          this.deleteModalErrMsg = '';
          this.deleteModalSuccessMsg = 'Successfully deleted your account';
        },
        (res) => {
          console.log(res);
          this.deleteModalErrMsg =
            'Failed to delete your account (server error)';
        }
      );
  }

  confirmUpdate() {
    const id: number = JWT.getIdFromToken();
    this.isLoading = true;

    if (id) {
      this.userService
        .updateUserById(
          id,
          this.username,
          this.email,
          this.zipcode,
          this.password,
          sessionStorage.getItem('token') || ''
        )
        .subscribe(
          (res) => {
            this.appComponent.updateUserData(
              res.body.username,
              this.appComponent.mineType,
              res.body.email,
              res.body.zipCode
            );
            this.updateModalSuccessMsg = 'Succesfully updated!';
            this.updateModalErrMsg = '';
            if (this.zipcode) {
              this.minerService
                .getWeatherByMinerId(
                  this.appComponent.minerId || 0,
                  sessionStorage.getItem('token') || ''
                )
                .subscribe((weatherRes) => {
                  this.appComponent.weather = weatherRes.body;
                  this.appComponent.weatherUrl = `http://openweathermap.org/img/wn/${this.appComponent.weather?.Icon}@2x.png`;
                }, console.log);
            }

            this.isLoading = false;
          },
          (res) => {
            this.updateModalErrMsg = 'Invalid input!';
            this.updateModalSuccessMsg = '';
            this.isLoading = false;
          }
        );
    }
  }

  confirmDelete() {
    if (this.confirmPasswordToDelete.trim() !== '') {
      const id: number = JWT.getIdFromToken();
      if (id) {
        this.authService
          .login(this.appComponent.username, this.confirmPasswordToDelete)
          .subscribe(
            (res) => {
              this.deleteUserForever(id);
            },
            (res) => {
              this.deleteModalErrMsg = 'Wrong password';
            }
          );
      } else {
        this.clickLogout();
      }
    } else {
      this.deleteModalErrMsg = 'Please enter your password';
    }
  }

  ngOnInit(): void {}
}
