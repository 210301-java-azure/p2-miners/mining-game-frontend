import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceCartComponent } from './resource-cart.component';

describe('ResourceCartComponent', () => {
  let component: ResourceCartComponent;
  let fixture: ComponentFixture<ResourceCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
