import { CraftedItem } from './CraftedItem';
import { Findable } from './Findable';

export interface Miner {
  id: number;
  copper: number;
  iron: number;
  oil: number;
  mine: {
    id: number;
    mineType: string;
    amountPerInterval: number;
  };
  craftedItems: CraftedItem[];
  findables: Findable[];
}
