export interface CraftedItem {
  name: string;
  amountPerIntervalMultiplier: number;
  weatherEffectsModifier: number;
  oilNeeded: number;
  ironNeeded: number;
  copperNeeded: number;
  description: string;
  cosmetic: boolean;
}
