import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CraftItemCardComponent } from './craft-item-card.component';

describe('CraftItemCardComponent', () => {
  let component: CraftItemCardComponent;
  let fixture: ComponentFixture<CraftItemCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CraftItemCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CraftItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
