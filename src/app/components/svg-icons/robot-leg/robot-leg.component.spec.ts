import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotLegComponent } from './robot-leg.component';

describe('RobotLegComponent', () => {
  let component: RobotLegComponent;
  let fixture: ComponentFixture<RobotLegComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RobotLegComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotLegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
