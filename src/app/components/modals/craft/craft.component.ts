import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { CraftedItem } from 'src/app/models/CraftedItem';
import { CraftedItemService } from 'src/app/services/crafted-item.service';
import { MineComponent } from '../../mine/mine.component';

@Component({
  selector: 'app-craft',
  templateUrl: './craft.component.html',
  styleUrls: ['./craft.component.css'],
})
export class CraftComponent implements OnInit {
  craftedItems?: CraftedItem[];
  craftErrMsg?: string;
  isLoading: boolean = true;

  constructor(
    appComponent: AppComponent,
    private mineComponent: MineComponent,
    craftedItemService: CraftedItemService
  ) {
    craftedItemService
      .getAllItemsByMinerId(appComponent.minerId || 0)
      .subscribe(
        (res) => {
          this.isLoading = false;
          this.craftedItems = res.body;
        },
        (res) => {
          this.isLoading = false;
          console.log(res);
        }
      );
  }

  clickToExit(): void {
    this.mineComponent.selectedSidebar = '';
  }

  ngOnInit(): void {}
}
