import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { MinerService } from 'src/app/services/miner.service';

@Component({
  selector: 'app-mine',
  templateUrl: './mine.component.html',
  styleUrls: ['./mine.component.css', './mine.css'],
})
export class MineComponent implements OnInit {
  selectedSidebar = '';

  itemlistTranslateAmount: number = -85;
  itemlistCurrentPage: number = 0;

  pickaxeRotationAmount: number = -20;

  constructor(
    public appComponent: AppComponent,
    private minerService: MinerService
  ) {}

  getItemlistTranslateAmount(): number {
    return this.itemlistTranslateAmount * this.itemlistCurrentPage;
  }

  clickUpArrow(): void {
    if (this.itemlistCurrentPage === 0) return;
    this.itemlistCurrentPage -= 1;
  }
  clickDownArrow(): void {
    if (this.itemlistCurrentPage === this.appComponent.minerItems.size - 2)
      return;

    this.itemlistCurrentPage += 1;
  }

  getPickaxeStyle() {
    return {
      transform: `rotate(${this.pickaxeRotationAmount}deg)`,
    };
  }

  handleSidebar(name: string) {
    switch (name) {
      case 'craft':
        this.selectedSidebar = 'craft';
        break;
      case 'inventory':
        this.selectedSidebar = 'inventory';
        break;
      case 'market':
        this.selectedSidebar = 'market';
        break;
      default:
        this.selectedSidebar = '';
    }
  }

  handleMousedownResource(): void {
    this.pickaxeRotationAmount = -70;
  }

  handleMouseupResource(): void {
    this.pickaxeRotationAmount = -20;
    switch (this.appComponent.mineType) {
      case 'OIL':
        this.appComponent.clickedOil += 0.1;
        break;
      case 'IRON':
        this.appComponent.clickedIron += 0.5;
        break;
      case 'COPPER':
        this.appComponent.clickedCopper += 1;
        break;
      default:
        console.log('Mine type is not specified');
    }
  }

  ngOnInit(): void {
    if (!this.appComponent.isMineComponentInitialized) {
      const updateWeatherInterval = setInterval(() => {
        if (this.appComponent.isLoggedIn) {
          this.minerService
            .getWeatherByMinerId(
              this.appComponent.minerId || 0,
              sessionStorage.getItem('token') || ''
            )
            .subscribe((res) => {
              this.appComponent.weather = res.body;
              this.appComponent.weatherUrl = `http://openweathermap.org/img/wn/${this.appComponent.weather?.Icon}@2x.png`;
            }, console.log);
        } else {
          clearInterval(updateWeatherInterval);
        }
      }, 1000 * 60);

      const updateClickedResourceInterval = setInterval(() => {
        if (this.appComponent.isLoggedIn) {
          const clickedResource: number =
            this.appComponent.clickedOil ||
            this.appComponent.clickedIron ||
            this.appComponent.clickedCopper;

          if (clickedResource) {
            this.minerService
              .updateClickedResource(
                this.appComponent.minerId || 0,
                clickedResource
              )
              .subscribe((res) => {
                this.appComponent.clickedCopper = 0;
                this.appComponent.clickedIron = 0;
                this.appComponent.clickedOil = 0;
                this.appComponent.updateUserResources(
                  res.body[0].oil,
                  res.body[0].iron,
                  res.body[0].copper
                );

                if (res.body[1]) {
                  this.appComponent.foundPart = res.body[1];
                  this.appComponent.isPartFound = true;
                  setTimeout(() => {
                    this.appComponent.isPartFound = false;
                  }, 5000);
                }
              }, console.log);
          }
        } else {
          clearInterval(updateClickedResourceInterval);
        }
      }, 30000);
    }

    this.appComponent.isMineComponentInitialized = true;
  }
}
