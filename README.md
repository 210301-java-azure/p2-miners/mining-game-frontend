# [Miner Might Frontend](http://minergamestorage.blob.core.windows.net/miner-game-blob/index.html)

With MinerMight, users are able to log in or register for an account where they will be provided with a miner (player account) and a mine. Players will be able to mine one of 3 resources: oil, iron, or copper. Players can then trade their resources to collect enough to craft items such as drills, trucks, and tarps. These items can improve efficiency of their mining or reduce the effects of weather, which link to the real world local weather of the player!

# UI Walkthrough

## Nav bar Categories

### [Profile](#profile-page)

### [Mine](#main-mining-page)

### [Leaderboard](#leaderboard-page)

## Log in and Register

The first page you land on to log in or register.
![Landing page with log in and register forms](/readme-images/landing.JPG)

Input validation messages.
![Landing page validation](/readme-images/landing-validation.JPG)

---

## Profile Page

The first page you see after logging in, you are able to view and edit your user profile.

![User Profile Page](/readme-images/profile.JPG)

To edit your account, a modal box appears for you to fill in new values.
![User Profile Update Modal](https://minergamestorage.blob.core.windows.net/miner-game-blob/readme-images/profile-update.JPG)

---

## Main Mining Page

The [main page](#base-page) of the application. Here you are able to view your total resources, [what items you own](#item-inventory) and [items you've found](#findable-item-inventory), click the minecart to manually mine resources and even have a chance to [find an item!](#found-something)

The buttons on the left let you [craft an item](#craft-an-item), view your inventory of [items](#item-inventory) and [findables](#findable-item-inventory), and [trade your resources](#trade-resources)

### Base Page

The main page. Click the minecart image to mine resources. You can also [find an item](#found-something) while mining!
![Mine page](/readme-images/mine.JPG)

### Craft an Item

Craft a new item. Each item has a diffferent cost and use.
![Crafting an item](/readme-images/craft-items.JPG)

### Item Inventory

View all the items you've crafted.
![Inventory of all items](/readme-images/inventory.JPG)

### Findable Item Inventory

Lists all the items you've found while mining.
![Inventory of findable items](/readme-images/findable-items.JPG)

### Trade Resources

You can trade your mined resources for the other two types, to craft items.
![Trade Resources](/readme-images/trade.JPG)

### Found Something

While clicking to mine, you can find robot parts!
![Found something while mining](/readme-images/found-new-item.JPG)

---

## Leaderboard Page

A leaderboard that determines the total value of each user and ranks them.

![Leaderboard page](/readme-images/leaderboard.JPG)
