import { Component } from '@angular/core';
import { JWT } from './jwt-util';
import { MinerService } from './services/miner.service';
import { UserService } from './services/user.service';
import { Weather } from './models/Weather';
import { CraftedItem } from './models/CraftedItem';
import { Findable } from './models/Findable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  isLoggedIn: boolean = false;
  isMineComponentInitialized: boolean = false;

  clickedOil: number = 0;
  clickedIron: number = 0;
  clickedCopper: number = 0;

  isPartFound = false;
  foundPart?: Findable;

  userId?: number;
  minerId?: number;
  mineId?: number;
  username: string = '';
  mineType: string = '';
  email: string = '';
  zipCode: string = '';

  efficiency: number = 0;

  minerItems = new Set();

  weather?: Weather;
  weatherUrl: string = '';

  oil: number = 0;
  iron: number = 0;
  copper: number = 0;

  resetFieldsUponLogout() {
    this.userId = 0;
    this.minerId = 0;
    this.mineId = 0;
    this.username = '';
    this.mineType = '';
    this.email = '';
    this.zipCode = '';
    this.minerItems = new Set();
    this.weatherUrl = '';
  }

  updateUserIds(userId: number, minerId: number, mineId: number) {
    this.userId = userId;
    this.minerId = minerId;
    this.mineId = mineId;
  }

  updateUserData(
    username: string,
    mineType: string,
    email: string,
    zipCode: string
  ): void {
    this.username = username;
    this.mineType = mineType;
    this.email = email;
    this.zipCode = zipCode;
  }

  updateUserResources(oil: number, iron: number, copper: number): void {
    this.oil = oil;
    this.iron = iron;
    this.copper = copper;
  }

  constructor(
    private userService: UserService,
    private minerService: MinerService
  ) {}

  ngOnInit(): void {
    const token: string | null = sessionStorage.getItem('token');
    if (token) {
      this.isLoggedIn = true;
      const tokenPayload: any = JWT.parseJwt(token);
      const id: number = +tokenPayload.jti;
      this.userId = id;
      this.userService
        .getUserById(this.userId, sessionStorage.getItem('token') || '')
        .subscribe((res) => {
          this.updateUserIds(
            res.body.id,
            res.body.miner.id,
            res.body.miner.mine.id
          );
          this.updateUserData(
            res.body.username,
            res.body.miner.mine.mineType,
            res.body.email,
            res.body.zipCode
          );
          this.updateUserResources(
            res.body.miner.oil,
            res.body.miner.iron,
            res.body.miner.copper
          );

          // update efficiency
          this.minerService
            .getEfficiencyByMinerId(
              this.minerId || 0,
              sessionStorage.getItem('token') || ''
            )
            .subscribe((res) => {
              this.efficiency = res.body;
            });

          // fill item name set
          res.body.miner.craftedItems.forEach((item: CraftedItem) => {
            this.minerItems.add(item.name);
          });

          // get weather
          this.minerService
            .getWeatherByMinerId(
              this.minerId || 0,
              sessionStorage.getItem('token') || ''
            )
            .subscribe((res) => {
              this.weather = res.body;
              this.weatherUrl = `http://openweathermap.org/img/wn/${this.weather?.Icon}@2x.png`;
            }, console.log);
        }, console.log);
    } else {
      sessionStorage.removeItem('token');
      this.isLoggedIn = false;
    }

    const resourceUpdateInterval = setInterval(() => {
      if (this.isLoggedIn) {
        this.userService
          .getUserById(this.userId || 0, sessionStorage.getItem('token') || '')
          .subscribe((res) => {
            this.updateUserResources(
              res.body.miner.oil,
              res.body.miner.iron,
              res.body.miner.copper
            );
          }, console.log);
      } else {
        sessionStorage.removeItem('token');
        this.isLoggedIn = false;
        clearInterval(resourceUpdateInterval);
      }
    }, 1000 * 60);
  }
}
