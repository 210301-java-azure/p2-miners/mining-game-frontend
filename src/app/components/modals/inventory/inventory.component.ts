import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { CraftedItem } from 'src/app/models/CraftedItem';
import { Findable } from 'src/app/models/Findable';
import { Miner } from 'src/app/models/Miner';
import { CraftedItemService } from 'src/app/services/crafted-item.service';
import { MinerService } from 'src/app/services/miner.service';
import { MineComponent } from '../../mine/mine.component';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
})
export class InventoryComponent implements OnInit {
  isLoading: boolean = true;

  minerData?: Miner;
  itemList?: CraftedItem[];
  findableList: Findable[] = [
    {
      name: 'Robot Head',
      rarity: 0.01,
      description: 'The rarest part!',
    },
    {
      name: 'Robot Torso',
      rarity: 0.05,
      description: 'The most metaly part!',
    },
    {
      name: 'Robot Arm',
      rarity: 0.1,
      description: 'How else is it gonna carry all your stuff?',
    },
    {
      name: 'Robot Leg',
      rarity: 0.1,
      description: 'Low horsepower, high torque!',
    },
  ];

  isInventoryScreen: boolean = true;

  inventoryItems = new Map();
  // { "Drill": 3, "Truck": 1,..}

  findableItems = new Map();

  constructor(
    private mineComponent: MineComponent,
    appComponent: AppComponent,
    minerService: MinerService,
    craftedItemService: CraftedItemService
  ) {
    this.findableItems.set('Robot Head', 0);
    this.findableItems.set('Robot Torso', 0);
    this.findableItems.set('Robot Arm', 0);
    this.findableItems.set('Robot Leg', 0);

    craftedItemService.getAllItems().subscribe(
      (res) => {
        this.itemList = res.body;
        this.itemList?.forEach((item) => {
          this.inventoryItems.set(item.name, 0);
        });

        minerService
          .getMinerById(
            appComponent.minerId || 0,
            sessionStorage.getItem('token') || ''
          )
          .subscribe(
            (minerRes) => {
              this.isLoading = false;
              this.minerData = minerRes.body;

              this.minerData?.craftedItems.forEach((item) => {
                this.inventoryItems.set(
                  item.name,
                  this.inventoryItems.get(item.name) + 1
                );
              });

              this.minerData?.findables.forEach((findable) => {
                this.findableItems.set(
                  findable.name,
                  this.findableItems.get(findable.name) + 1
                );
              });
            },
            (res) => {
              this.isLoading = false;
              console.log(res);
            }
          );
      },
      (res) => {
        this.isLoading = false;
        console.log(res);
      }
    );
  }

  clickTreasureBtn() {
    this.isInventoryScreen = false;
  }
  clickInventoryBtn() {
    this.isInventoryScreen = true;
  }

  clickToExit(): void {
    this.mineComponent.selectedSidebar = '';
  }

  ngOnInit(): void {}
}
