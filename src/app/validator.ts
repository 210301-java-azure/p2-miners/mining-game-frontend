export class Validator {
  static validateEmail(email: string): boolean {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.trim().toLowerCase());
  }

  static validateZipCode(zipCode: string): boolean {
    const re = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
    return re.test(zipCode);
  }

  static validatePassLength(password: string, min: number): boolean {
    return password.trim().length >= min;
  }

  static confirmPassword(password: string, password2: string): boolean {
    return password === password2;
  }
}
