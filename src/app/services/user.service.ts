import { HttpClient } from '@angular/common/http';
import { Token, tokenize } from '@angular/compiler/src/ml_parser/lexer';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  userUrl = environment.baseUrl + '/users';
  mineUrl = environment.baseUrl + '/mines';

  getUserById(id: number, token: string): Observable<any> {
    return this.http.get<any>(this.userUrl + `/${id}`, {
      headers: { 'miner-token': token },
      observe: 'response',
    });
  }

  deleteUserById(id: number, token: string): Observable<any> {
    return this.http.delete<any>(this.userUrl + `/${id}`, {
      headers: { 'miner-token': token },
    });
  }

  updateUserById(
    id: number,
    username: string | null,
    email: string | null,
    zipCode: string | null,
    password: string | null,
    token: string, 
  ): Observable<any> {
    
    if(username == '')username = null;
    if(email == '')email = null;
    if(password == '')password = null;
    if(zipCode == '')zipCode = null;
    const payload = {username, email, zipCode, password};
    return this.http.put<any>(this.userUrl + `/${id}`, payload, {
      headers: {'miner-token': token},
      observe: 'response',
    });
  }
}
