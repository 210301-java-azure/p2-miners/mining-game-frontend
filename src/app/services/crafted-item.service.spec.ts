import { TestBed } from '@angular/core/testing';

import { CraftedItemService } from './crafted-item.service';

describe('CraftedItemService', () => {
  let service: CraftedItemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CraftedItemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
