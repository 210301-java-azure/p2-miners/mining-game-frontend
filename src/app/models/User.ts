import { Miner } from './Miner';

export interface User {
  id: number;
  username: string;
  email: string;
  miner: Miner;
  zipCode: string;
}
