import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RobotHeadComponent } from './robot-head.component';

describe('RobotHeadComponent', () => {
  let component: RobotHeadComponent;
  let fixture: ComponentFixture<RobotHeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RobotHeadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RobotHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
