import { Component, Input, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { CraftedItem } from 'src/app/models/CraftedItem';
import { CraftedItemService } from 'src/app/services/crafted-item.service';
import { MinerService } from 'src/app/services/miner.service';

@Component({
  selector: 'app-craft-item-card',
  templateUrl: './craft-item-card.component.html',
  styleUrls: ['./craft-item-card.component.css'],
})
export class CraftItemCardComponent implements OnInit {
  @Input() item?: CraftedItem;

  isDescriptionShown: boolean = false;

  isCrafting: boolean = false;
  craftMeterPercent: number = 0;

  craftErrMsg?: string;

  constructor(
    private appComponent: AppComponent,
    private minerService: MinerService,
    private craftedItemService: CraftedItemService
  ) {}

  showDescription() {
    this.isDescriptionShown = true;

    setTimeout(() => {
      this.isDescriptionShown = false;
    }, 3000);
  }

  clickCraftBtn(itemName: string): void {
    this.craftedItemService
      .craftItem(this.appComponent.minerId || 0, itemName)
      .subscribe(
        (res) => {
          this.isCrafting = true;
          const meterInterval = setInterval(() => {
            if (this.craftMeterPercent < 100) this.craftMeterPercent += 0.2;
            else {
              clearInterval(meterInterval);
              this.isCrafting = false;
              this.craftMeterPercent = 0;
            }
          }, 5);

          // update efficiency
          this.minerService
            .getEfficiencyByMinerId(
              this.appComponent.minerId || 0,
              sessionStorage.getItem('token') || ''
            )
            .subscribe((effRes) => {
              setTimeout(() => {
                this.appComponent.efficiency = effRes.body;
              }, (100 / 0.2) * 5);
            });

          this.minerService
            .getMinerById(
              this.appComponent.minerId || 0,
              sessionStorage.getItem('token') || ''
            )
            .subscribe((minerRes) => {
              this.appComponent.updateUserResources(
                minerRes.body.oil,
                minerRes.body.iron,
                minerRes.body.copper
              );

              setTimeout(() => {
                minerRes.body.craftedItems.forEach((item: CraftedItem) => {
                  this.appComponent.minerItems.add(item.name);
                });
              }, (100 / 0.2) * 5);
            }, console.log);
        },
        (res) => {
          this.craftErrMsg = res.error.message;
        }
      );
  }

  ngOnInit(): void {}
}
