import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { CraftedItem } from 'src/app/models/CraftedItem';
import { AuthService } from 'src/app/services/auth.service';
import { MinerService } from 'src/app/services/miner.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';

  loginErrMsg: string = '';
  isLoading: boolean = false;

  constructor(
    private authService: AuthService,
    private minerService: MinerService,
    private appComponent: AppComponent
  ) {}

  clickLogin() {
    if (this.username.trim() === '' || this.password.trim() === '') {
      this.loginErrMsg = 'Failed to login';
      return;
    }

    this.isLoading = true;
    this.loginErrMsg = '';
    this.authService.login(this.username, this.password).subscribe(
      (res) => {
        this.isLoading = false;
        const token: string = res.headers.get('miner-token');
        sessionStorage.setItem('token', token);
        this.appComponent.isLoggedIn = true;
        this.appComponent.updateUserIds(
          res.body.id,
          res.body.miner.id,
          res.body.miner.mine.id
        );
        this.appComponent.updateUserData(
          res.body.username,
          res.body.miner.mine.mineType,
          res.body.email,
          res.body.zipCode
        );
        this.appComponent.updateUserResources(
          res.body.miner.oil,
          res.body.miner.iron,
          res.body.miner.copper
        );
        // update efficiency
        this.minerService
          .getEfficiencyByMinerId(this.appComponent.minerId || 0, token)
          .subscribe((res) => {
            this.appComponent.efficiency = res.body;
          });

        // fill item name set
        res.body.miner.craftedItems.forEach((item: CraftedItem) => {
          this.appComponent.minerItems.add(item.name);
        });

        // get weather
        this.minerService
          .getWeatherByMinerId(
            this.appComponent.minerId || 0,
            sessionStorage.getItem('token') || ''
          )
          .subscribe((res) => {
            this.appComponent.weather = res.body;
            this.appComponent.weatherUrl = `http://openweathermap.org/img/wn/${this.appComponent.weather?.Icon}@2x.png`;
          }, console.log);
      },
      (res) => {
        this.isLoading = false;
        this.loginErrMsg = 'Failed to login';
      }
    );
    this.username = '';
    this.password = '';
  }

  ngOnInit(): void {}
}
