import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { CraftedItem } from 'src/app/models/CraftedItem';
import { AuthService } from 'src/app/services/auth.service';
import { MinerService } from 'src/app/services/miner.service';
import { Validator } from 'src/app/validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  isLoading: boolean = false;

  username: string = '';
  email: string = '';
  zipCode: string = '';
  password: string = '';
  confirmPassword: string = '';
  mineType: string = '0';

  mineTypesToSelect = ['OIL', 'IRON', 'COPPER'];
  inputsArr: string[] = [
    'username',
    'email',
    'zip-code',
    'password',
    'confirm-password',
    'mine-type',
  ];

  registerErrMsg: string = '';
  errMsgArr: string[] = ['', '', '', '', '', ''];

  constructor(
    private authService: AuthService,
    private minerService: MinerService,
    private appComponent: AppComponent
  ) {}

  validateInput(input: string): void {
    switch (input) {
      case 'username':
        if (this.username.trim() === '')
          this.errMsgArr[0] = 'Username cannot be empty';
        else this.errMsgArr[0] = '';
        break;

      case 'email':
        if (Validator.validateEmail(this.email.trim())) this.errMsgArr[1] = '';
        else this.errMsgArr[1] = 'Email is invalid';
        break;

      case 'zip-code':
        if (Validator.validateZipCode(this.zipCode)) this.errMsgArr[2] = '';
        else this.errMsgArr[2] = 'Zip Code is invalid';
        break;

      case 'password':
        const minLength = 4;
        if (Validator.validatePassLength(this.password, minLength))
          this.errMsgArr[3] = '';
        else
          this.errMsgArr[3] = `Password must be at least ${minLength} characters`;
        break;

      case 'confirm-password':
        if (Validator.confirmPassword(this.password, this.confirmPassword))
          this.errMsgArr[4] = '';
        else this.errMsgArr[4] = "Password doesn't match";
        break;

      case 'mine-type':
        if (this.mineType !== '0') this.errMsgArr[5] = '';
        else this.errMsgArr[5] = 'Please pick your mine type';
        break;
      default:
        return;
    }
  }

  clickRegister(): void {
    this.registerErrMsg = '';
    this.inputsArr.forEach((input) => this.validateInput(input));
    const errLength: number = this.errMsgArr.filter((msg) => msg !== '').length;

    if (errLength) return;

    this.isLoading = true;

    this.authService
      .register(
        this.username,
        this.email,
        this.zipCode,
        this.password,
        this.mineType
      )
      .subscribe(
        (res) => {
          this.authService.login(res.body.username, this.password).subscribe(
            (loginRes) => {
              this.isLoading = false;
              const token: string = loginRes.headers.get('miner-token');
              sessionStorage.setItem('token', token);
              this.appComponent.isLoggedIn = true;
              this.appComponent.updateUserIds(
                res.body.id,
                res.body.miner.id,
                res.body.miner.mine.id
              );
              this.appComponent.updateUserData(
                res.body.username,
                res.body.miner.mine.mineType,
                res.body.email,
                res.body.zipCode
              );
              this.appComponent.updateUserResources(
                res.body.miner.oil,
                res.body.miner.iron,
                res.body.miner.copper
              );

              // update efficiency
              this.minerService
                .getEfficiencyByMinerId(this.appComponent.minerId || 0, token)
                .subscribe((effRes) => {
                  this.appComponent.efficiency = effRes.body;
                });

              // fill item name set
              loginRes.body.miner.craftedItems.forEach((item: CraftedItem) => {
                this.appComponent.minerItems.add(item.name);
              });

              // get weather
              this.minerService
                .getWeatherByMinerId(
                  this.appComponent.minerId || 0,
                  sessionStorage.getItem('token') || ''
                )
                .subscribe((weatherRes) => {
                  this.appComponent.weather = weatherRes.body;
                  this.appComponent.weatherUrl = `http://openweathermap.org/img/wn/${this.appComponent.weather?.Icon}@2x.png`;
                }, console.log);
            },
            (loginRes) => {
              this.isLoading = false;
              console.log(loginRes);
            }
          );
        },
        (res) => {
          this.isLoading = false;
          this.registerErrMsg = res.error.message;
        }
      );
  }

  ngOnInit(): void {}
}
