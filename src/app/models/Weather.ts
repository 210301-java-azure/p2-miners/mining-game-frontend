export interface Weather {
  Temp: number;
  'Effect Reduction': number;
  'Weather effect': number;
  Icon: string;
}
