export interface Findable {
  name: string;
  rarity: number;
  description: string;
}
