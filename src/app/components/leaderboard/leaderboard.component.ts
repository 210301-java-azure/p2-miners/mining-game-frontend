import { Component, OnInit } from '@angular/core';
import { MinerService } from 'src/app/services/miner.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css'],
})
export class LeaderboardComponent implements OnInit {
  minerList?: { username: string; score: number }[];
  isLoading: boolean = true;

  constructor(minerService: MinerService) {
    minerService.getLeaderboard().subscribe(
      (res) => {
        this.isLoading = false;
        this.minerList = res.body;
      },
      (res) => {
        this.isLoading = false;
        console.log(res);
      }
    );
  }

  ngOnInit(): void {}
}
