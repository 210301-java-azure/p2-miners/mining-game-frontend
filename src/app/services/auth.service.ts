import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  loginUrl: string = environment.baseUrl + '/login';
  registerUrl: string = environment.baseUrl + '/users';

  login(username: string, password: string): Observable<any> {
    const payload = { username, password };
    return this.http.post<any>(this.loginUrl, payload, { observe: 'response' });
  }

  register(
    username: string,
    email: string,
    zipCode: string,
    password: string,
    mineType: string
  ): Observable<any> {
    const payload = { username, email, zipCode, password };
    return this.http.post<any>(this.registerUrl, payload, {
      headers: { 'mine-type': mineType },
      observe: 'response',
    });
  }
}
