import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarpsComponent } from './tarps.component';

describe('TarpsComponent', () => {
  let component: TarpsComponent;
  let fixture: ComponentFixture<TarpsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarpsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
